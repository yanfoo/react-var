import React from "react";
import ReactDOMServer from "react-dom/server";
import { render, screen, act } from "@testing-library/react";

import type { ReactVar } from "../index";
import { createReactVar } from "../index";

describe("Testing entry point", () => {
  it("should expose provider and hooks", () => {
    expect(createReactVar).toBeInstanceOf(Function);
  });

  describe("Testing createReactVar", () => {
    it("should create instance", () => {
      const v = createReactVar("foo");

      expect(v).toBeInstanceOf(Function);
      expect(v.name).toBe("ReactVar");
      expect(v.value).toEqual("foo");
    });

    it("should create with init function", () => {
      const v = createReactVar(() => "bar");

      expect(v).toBeInstanceOf(Function);
      expect(v.value).toEqual("bar");
    });

    it("should not allow modifying value directly", () => {
      const v = createReactVar("foo");

      expect(() => (v.value = "bar")).toThrow("Cannot set property value of");
    });

    it("should notify from subscription", async () => {
      const v = createReactVar("foo");
      let __prevValue;
      let __newValue;

      v.subscribe(({ prevValue, value }) => {
        __prevValue = prevValue;
        __newValue = value;
      });

      await v("bar");

      expect(v.value).toBe("bar");
      expect(__newValue).toBe("bar");
      expect(__prevValue).toBe("foo");
    });

    it("should not recursively modify value", async () => {
      const v = createReactVar("foo");

      v.subscribe(() => {
        return expect(() => v("another")).rejects.toThrow(
          "Cannot modify ReactVar while it is already being modified!"
        );
      });

      await v("bar");
    });

    it("should stack concurrent mutations", async () => {
      const v = createReactVar(0);
      const m: number[] = [];

      v.subscribe(({ value }) => {
        m.push(value);
      });

      await Promise.all([v(async () => 1), v(2), v(async () => 3)]);

      expect(v.value).toBe(3);
      expect(m).toEqual([1, 2, 3]);
    });

    it("should unsubscribe", async () => {
      const v = createReactVar("foo");
      const handler = () => counter++;
      let counter = 0;

      expect(v.value).toBe("foo");

      v.subscribe(handler);
      await v("bar");
      expect(v.value).toBe("bar");
      expect(counter).toBe(1);
    });

    it("should error when stacking modifications", async () => {
      const v = createReactVar("foo");

      expect(
        Promise.all(
          Array.from({ length: 1000 }).map((_, index) => v("foo" + index))
        )
      ).rejects.toThrow("Maximum call stack reached for modifying ReactVar!");
    });

    it("should invoke callbacks after modifcations", async () => {
      const v = createReactVar(0, {
        comparator: (a, b) => Math.abs(a - b) < 0.5,
      });
      const steps: number[] = [];
      let stepsLen = 0;

      v.subscribe(({ value, after }) => {
        steps.push(value);

        after(() => v((value) => value / 2));
      });

      await v(16);

      // wait while there are changes...
      while (steps.length !== stepsLen) {
        stepsLen = steps.length;
        await new Promise((resolve) => setTimeout(resolve));
      }

      // the following values represent all of the changes notified
      expect(steps).toEqual([16, 8, 4, 2, 1, 0.5]);
      expect(v.value).toBe(0.25); // 0.5 == 0.25 according to the comparator
    });
  });

  describe("Testing useValue", () => {
    type TestContainerProps<T, V> = {
      reactVar: ReactVar<T>;
      predicate?: (value: T) => V;
      onRender: () => void;
    };

    const TestContainer = <T, V>({
      reactVar,
      predicate,
      onRender,
    }: TestContainerProps<T, V>) => {
      const value = reactVar.useValue(predicate);

      onRender();

      return <div data-testid="output">{String(value)}</div>;
    };

    it("should output value", async () => {
      const v = createReactVar("foo");
      let updateCount = 0;

      await act(async () => {
        render(<TestContainer reactVar={v} onRender={() => updateCount++} />);
      });

      expect(screen.getByTestId("output")).toHaveTextContent("foo");
      expect(updateCount).toBe(1);
    });

    it("should output value (SSR)", async () => {
      const v = createReactVar("foo");
      let updateCount = 0;

      await act(async () => {
        const ui = (
          <TestContainer reactVar={v} onRender={() => updateCount++} />
        );
        const container = document.createElement("div");
        document.body.appendChild(container);
        container.innerHTML = ReactDOMServer.renderToString(ui);

        render(ui, { hydrate: true, container });
      });

      expect(screen.getByTestId("output")).toHaveTextContent("foo");
      expect(updateCount).toBe(2);
    });

    it("should update output value", async () => {
      const v = createReactVar("foo");
      let updateCount = 0;

      await act(async () => {
        render(<TestContainer reactVar={v} onRender={() => updateCount++} />);
      });

      expect(screen.getByTestId("output")).toHaveTextContent("foo");
      expect(updateCount).toBe(1);

      await act(async () => {
        await v((value) => {
          expect(value).toBe("foo");

          return "bar";
        });

        expect(v.value).toBe("bar");
      });

      expect(screen.getByTestId("output")).toHaveTextContent("bar");
      expect(updateCount).toBe(2);
    });

    it("should output value from predicate", async () => {
      const v = createReactVar({ testProperty: "foo" });
      let updateCount = 0;

      await act(async () => {
        render(
          <TestContainer
            reactVar={v}
            predicate={(value) => `${value.testProperty.toLocaleUpperCase()}!`}
            onRender={() => updateCount++}
          />
        );
      });

      await act(async () => {
        await v({ testProperty: "foo" }); // post update, don't trigger
      });

      expect(updateCount).toBe(1);

      await act(async () => {
        await v({ testProperty: "bar" }); // post update, trigger re-render
      });

      expect(updateCount).toBe(2);

      expect(screen.getByTestId("output")).toHaveTextContent("BAR!");
    });

    // it('should output value from predicate (SSR', async () => {
    //    const v = createReactVar({ testProperty:'foo' });
    //    let updateCount = 0;

    //    await act(async () => {
    //       render(<TestContainer
    //          reactVar={ v }
    //          predicate={ value => `${value.testProperty.toLocaleUpperCase()}!` }
    //          onRender={ () => updateCount++ }
    //       />, { hydrate: true });
    //    });

    //    await v({ testProperty:'foo' }); // post update, don't trigger
    //    expect(updateCount).toBe(2);

    //    await v({ testProperty:'bar' }); // post update, trigger re-render
    //    expect(updateCount).toBe(3);

    //    expect(screen.getByTestId('output')).toHaveTextContent('BAR!');
    // });
  });

  describe("Testing options", () => {
    it("should not notify subscribers with custom comparator", async () => {
      const v1 = createReactVar("foo", {
        comparator: () => true,
      });
      const v2 = createReactVar("foo", {
        comparator: () => false,
      });
      let sub1 = false;
      let sub2 = false;

      const unsub1 = v1.subscribe(() => (sub1 = true));
      const unsub2 = v2.subscribe(() => (sub2 = true));

      await v1("bar");
      await v2("bar");

      unsub1();
      unsub2();

      // value changed, but subscribers not notified (because same value from comparator)
      expect(v1.value).toBe("bar");
      expect(sub1).toBe(false);

      // value changed, and subscribers notified (because different values from comparator)
      expect(v2.value).toBe("bar");
      expect(sub2).toBe(true);
    });

    it("should not update value with custom transform", async () => {
      const v1 = createReactVar("foo", {
        comparator: () => true,
        transform: (newVal, oldVal, diff) => (diff ? newVal : oldVal),
      });
      const v2 = createReactVar("foo", {
        comparator: () => false,
        transform: (newVal, oldVal, diff) => (diff ? newVal : oldVal),
      });
      let sub1 = false;
      let sub2 = false;

      const unsub1 = v1.subscribe(() => (sub1 = true));
      const unsub2 = v2.subscribe(() => (sub2 = true));

      await v1("bar");
      await v2("bar");

      unsub1();
      unsub2();

      // 'bar' is considered equals to 'foo', nothing changed
      expect(v1.value).toBe("foo");
      expect(sub1).toBe(false);

      // 'bar' is considered deifferent to 'foo', value changed, subs notified
      expect(v2.value).toBe("bar");
      expect(sub2).toBe(true);
    });

    it("should not update concurrently", async () => {
      const v = createReactVar("foo", {
        nonconcurrent: true,
      });

      v(async () => {
        await new Promise((resolve) => setTimeout(resolve, 10));

        return "bar";
      });
      expect(v("buz")).rejects.toThrow("Nonconcurrent ReactVar!");
    });
  });
});
