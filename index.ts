import { useSyncExternalStore } from "react";

export type ComparatorFunction<T> = {
  (newVal: T, oldVal: T): boolean;
};
export type TransformFunction<T> = {
  (newVal: T, oldVal: T, changed: boolean): T;
};

export type ReactVarOptions<T> = {
  comparator?: ComparatorFunction<T>;
  transform?: TransformFunction<T>;
  nonconcurrent?: boolean;
};

export type SubscriptionHandlerArgs<T> = {
  prevValue: T;
  value: T;
  after(callback: () => void): void;
};
export type SubscriptionHandler<T> = {
  (args: SubscriptionHandlerArgs<T>): Promise<unknown | void> | unknown | void;
};

export type UseValuePredicate<T, V> = {
  (value: T): V;
};

export type InitialValueArg<T> =
  | {
      (): T;
    }
  | T;

export type ChangedValueArg<T> =
  | {
      (oldValue: T): Promise<T> | T;
    }
  | T;

export type ReactVar<T> = {
  /**
   * Set a new value and notify subscribers
   */
  (newValue?: ChangedValueArg<T>): Promise<ReactVar<T>>;
  /**
   * Get the current value
   */
  value: T;
  /**
   * Register a new subscriber. Returns a function to unsubsribe the handler
   * @param handler will be called with the updated value
   */
  subscribe(handler: SubscriptionHandler<T>): () => void;
  /**
   * Reactively fetch value from this variable
   */
  useValue<V = T>(predicate?: UseValuePredicate<T, V>): V;
};

const MAX_CALL_STACK = 200;

/**
 *
 * @param initialValue
 * @param options
 * @returns a new instance of ReactVar
 */
const reactVar = <T>(
  initialValue: InitialValueArg<T>,
  options?: ReactVarOptions<T>
): ReactVar<T> => {
  const modQueue: (() => void)[] = [];
  const subscribers = new Set<SubscriptionHandler<T>>();
  const callbacks = new Set<{ (): void }>();
  const comparator = options?.comparator ?? ((n, o) => n === o);
  const transform = options?.transform ?? ((v) => v);
  const nonconcurrent = options?.nonconcurrent ?? false;
  const after = (callback: { (): void }) => {
    callbacks.add(callback);
  };

  let value = initialValue instanceof Function ? initialValue() : initialValue;
  let active = false;
  let locked = false;

  const setValue = async (newValue: T) => {
    const prevValue = value;

    if (locked)
      throw new Error(
        "Cannot modify ReactVar while it is already being modified!"
      );

    if (active) {
      if (nonconcurrent) {
        throw new Error("Nonconcurrent ReactVar!");
      } else if (modQueue.length > MAX_CALL_STACK) {
        throw new Error("Maximum call stack reached for modifying ReactVar!");
      }

      // block until the previous call resumes
      await new Promise<void>((resolve) => modQueue.push(resolve));
    }

    active = true;

    if (newValue instanceof Function) {
      newValue = await newValue(value);
    }

    const valueChanged = !comparator(newValue, value);
    value = await transform(newValue, value, valueChanged);

    if (valueChanged && subscribers.size) {
      locked = true;

      for (const handler of subscribers) {
        await handler({ prevValue, value, after });
      }
      locked = false;
    }

    if (modQueue.length > 0) {
      (modQueue.shift() as () => void)(); // resume next call
    } else {
      active = false;

      if (callbacks.size) {
        const _callbacks = Array.from(callbacks);
        callbacks.clear();
        _callbacks.forEach((callback) => callback());
      }
    }

    return instance;
  };

  const instance = async function ReactVar(newValue: T) {
    return setValue(newValue);
  } as ReactVar<T>;

  Object.defineProperties(instance, {
    value: {
      configurable: false,
      enumerable: true,
      get: () => value,
    },
    subscribe: {
      configurable: false,
      enumerable: false,
      writable: false,
      value: (handler: SubscriptionHandler<T>) => {
        subscribers.add(handler);
        return () => subscribers.delete(handler);
      },
    },
    useValue: {
      configurable: false,
      enumerable: true,
      value: <V = T>(predicate?: UseValuePredicate<T, V>) => {
        return useSyncExternalStore<T | V>(
          (updateValue: () => void) =>
            instance.subscribe(({ prevValue, value }) => {
              if (
                !predicate ||
                JSON.stringify(predicate(prevValue)) !==
                  JSON.stringify(predicate(value))
              ) {
                updateValue();
              }
            }),
          () => (predicate ? predicate(value) : value),
          () => (predicate ? predicate(value) : value)
        );
      },
    },
  });

  return Object.freeze(instance);
};

/**
 * Create a new ReactVar instance
 * @param initialValue the initial value
 * @param options the options
 * @returns the new ReactVar instance
 */
const createReactVar = <T>(
  initialValue: InitialValueArg<T>,
  options?: ReactVarOptions<T>
) => reactVar(initialValue, options);

export default createReactVar;
export { createReactVar };
